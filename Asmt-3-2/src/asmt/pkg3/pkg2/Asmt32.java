package asmt.pkg3.pkg2;

import java.io.File;
import java.io.PrintStream;
import java.util.Scanner;

public class Asmt32 {

    public static void main(String[] args) {

        //declare the printstream
        PrintStream out = null;
        //init the print stream
        try {
            out = new PrintStream(new File("Asmt_3-1_Output.txt"));
        } catch (Exception e) {
            System.err.println("Bad things");
        }
        //declare and init the scanner
        Scanner userIn = new Scanner(System.in);
        //loop for 10 cycles to get 10 ints from the user
        for (int i = 0; i < 10; i++) {
            System.out.println("Input a whole number");
            out.println(userIn.nextInt());
        }

    }

}
